//
//  ViewProductViewController.swift
//  SantoshSangal
//
//  Created by My Mac on 10/07/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import iOSDropDown
import IBAnimatable


class ViewProductViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate  {
    
    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var btncart: SSBadgeButton!
    @IBOutlet var tblproductlist: UITableView!
    
//    var temparr = [String]()
//     var pricetemparr = [String]()
     var productlist = [[String:Any]]()
//    var  subcategorydictdata = [String:Any]()
//    var quntyarr = (1...50).map{"\($0)"}
//    var isfromposter = false
//    var isfromsearch = false
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
     //   lbltitle.text = subcategorydictdata["Name"] as? String
        
        tblproductlist.tableFooterView = UIView()
        LocalProductApi()
       // btncart.badge = String(GlobalVariables.globalarray.count)
      //  btncart.badge = String(99)
      //  btncart.addBadge(number: 99, addedView: btncart)
//        if isfromposter {
//            OfferProductApi()
//        }
//        else
//        {
//            if isfromsearch {
//
//            }
//            else
//            {
//               ProductApi()
//            }
//
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
       //  btncart.badge = String(GlobalVariables.globalarray.count)
    }
    
   
    
        //MARK: - Tableview Data Source
             
             func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
              
                return productlist.count
                
             }
             
             func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
              
                 let cell = tableView.dequeueReusableCell(withIdentifier: "productcell", for: indexPath) as! productcell
                
                cell.lbltitle.text = productlist[indexPath.row]["ProductName"] as? String
                
                if productlist[indexPath.row]["isDealOfDay"] as? String ?? "" == "1"{
                    
                    cell.lblprice.attributedText = String.makeSlashText("RS \(productlist[indexPath.row]["Dis_price"] as? String ?? "")") //
                    cell.lblofferprice.text = "RS \(productlist[indexPath.row]["Price"] as? String ?? "")"
                }
                else
                {
                    cell.lblofferprice.text = ""
                    cell.lblprice.text = "RS \(productlist[indexPath.row]["Price"] as? String ?? "")"
                }
                
                
                let imgURL = productlist[indexPath.row]["ProductImage"] as? String ?? ""
                                     cell.imgphoto.sd_setImage(with: URL(string:imgURL), placeholderImage: #imageLiteral(resourceName: "Logo"), options:.progressiveLoad, completed: nil)
                
              //  cell.btnaddcart.tag = indexPath.row
               
//                cell.txtqty.tag = indexPath.row
//                 cell.txtqty.delegate = self
                
               // cell.txtqty.addTarget(self, action: #selector(typingmainprice), for: .editingChanged)
                             
              //  let isIndexValid = temparr.indices.contains(indexPath.row)
              //  if isIndexValid {
                 //   cell.txtqty.text = temparr[indexPath.row]
//                }
//                else
//                {
//                     cell.txtqty.text = ""
//                }
                
//                cell.btnaddcart.addTarget(self, action:#selector(addcartclick(_:event:)), for: .touchUpInside)
                
//                 cell.btnCartAction = { () in
//
//                    print("QNTY:",cell.txtqty.text ?? "")
//                     print("cell:",indexPath.row)
//
//                     if cell.txtqty?.text == "Qty" || cell.txtqty?.text == "" {
//                                showToast(uiview: self, msg:"Enter Quantity")
//                            }
//                            else
//                            {
//
//                                var dict:[String:Any] = self.productlist[indexPath.row]
//                                dict["qty"] = cell.txtqty.text
////                                if !GlobalVariables.globalarray.contains{ $0 == dict as! [String : Any] } {
////                                    GlobalVariables.globalarray.append(dict)
////                                }
//                            //    let globalarr:[[String:Any]] = GlobalVariables.globalarray
//
//
//                                let index = GlobalVariables.globalarray.firstIndex(where: { dictionary in
//                                                                     guard let value = dictionary["ProductID"] as? String
//                                                                       else { return false }
//                                                                     return value == dict["ProductID"] as? String
//                                                                   })
//                                                                   if let index = index {
//                                                                       GlobalVariables.globalarray.remove(at: index)
//                                                                    GlobalVariables.globalarray.append(dict)
//                                                                   }
//                                else
//                                                                   {
//                                                                     GlobalVariables.globalarray.append(dict)
//                                }
//
////                                if GlobalVariables.globalarray.contains(where: {NSDictionary(dictionary: $0).isEqual(to: dict)}) {
////
////                                      GlobalVariables.globalarray.append(dict)
////
////                                }
////                                else
////                                {
////                                     GlobalVariables.globalarray.append(dict)
////                                }
//
//
//                    //            let saveddata = ["Qty":sender.titleLabel?.text ?? "",
//                    //                               //    "textviewdecription":textviewdecription.text ?? "",
//                    //                "items":dict
//                    //                      ] as [String : Any]
//                    //
//                    //                  UserDefaults.standard.set(saveddata, forKey: "\(dict["CategoryID"]!)\(dict["SubcategoryID"]!)\(dict["ProductID"]!)")
//                                showToast(uiview: self, msg:"Product Added Successfully")
//                                UserDefaults.standard.set(GlobalVariables.globalarray, forKey: "items")
//                                      UserDefaults.standard.synchronize()
//                                self.viewWillAppear(true)
//                               // self.btncart.badge = String(GlobalVariables.globalarray.count)
//                            }
//                }
                
//                let Details = productlist[indexPath.row]["Details"] as? [[String:Any]]
//
//                if Details?.count ?? 0 > 0 {
//                    cell.lbltotalprice.isHidden = false
//                    cell.txtqty.optionArray =  Details!.compactMap {"\( $0["pktsize"] as? String ?? "0") \( $0["uomdesc"] as? String ?? "0")"}
//                    cell.lbltotalprice.text = pricetemparr[indexPath.row]
//                     cell.txtqty.isSearchEnable = false
//
//                }
//                else
//                {
//                    cell.txtqty.optionArray =  quntyarr
//                    cell.txtqty.isSearchEnable = true
//                      cell.lbltotalprice.isHidden = true
//                }
                
                
              //  cell.txtqty.optionIds =  quntyarr.compactMap {Int( $0["CategoryID"] as? String ?? "0")}
//   cell.txtqty.didSelect(completion: { (selected, index, id)  in
//          print(index,id)
//    print("cell:",indexPath.row)
//    if Details?.count ?? 0 > 0
//    {
//        cell.lbltotalprice.text = "RS \(Details?[index]["price"] as? String ?? "0")"
//        self.pricetemparr[cell.txtqty.tag] = "RS \(Details?[index]["price"] as? String ?? "0")"
//    }
//    else
//    {
//    }
//   // self.insertElementAtIndex(element: selected, indexx: indexPath.row)
//   // self.temparr.insert(selected, at: index)
//    self.temparr[cell.txtqty.tag] = selected
//      })
                 return cell
    }
    
    
//    @objc func typingmainprice(textField:UITextField){
//        //   let dict = productlist[textField.tag] as [String:Any]
//
//           if let typedText = textField.text {
//            //  self.insertElementAtIndex(element:typedText, indexx: textField.tag)
//              // temparr.insert(typedText, at: textField.tag)
//             self.temparr[textField.tag] = typedText
//           }
//       }
//
////        func insertElementAtIndex(element: String?, indexx: Int) {
////
////            while temparr.count <= indexx {
////                temparr.append("")
////            }
////
////            temparr.insert(element!, at: indexx)
////        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
              self.popupAlert(title: "", message: "add product to cart please login first!", actionTitles: ["NO","YES"], actions:[{action1 in

                              },{action2 in
                                   self.navigationController?.popViewController(animated: true)
                                  //self.calcamt()
                              }, nil])

        }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
//           let compSepByCharInSet = string.components(separatedBy: aSet)
//           let numberFiltered = compSepByCharInSet.joined(separator: "")
//       // self.insertElementAtIndex(element: textField.text, indexx: textField.tag)
//           return string == numberFiltered
//    }
//
//
//      @objc func addcartclick(_ sender: UIButton, event: Any){
//
//        if sender.titleLabel?.text == "Qty" || sender.titleLabel?.text == "" {
//            showToast(uiview: self, msg:"Enter Quantity")
//        }
//        else
//        {
//
//            let dict:[String:Any] = productlist[sender.tag]
//             GlobalVariables.globalarray.append(dict)
////            let saveddata = ["Qty":sender.titleLabel?.text ?? "",
////                               //    "textviewdecription":textviewdecription.text ?? "",
////                "items":dict
////                      ] as [String : Any]
////
////                  UserDefaults.standard.set(saveddata, forKey: "\(dict["CategoryID"]!)\(dict["SubcategoryID"]!)\(dict["ProductID"]!)")
////                //  UserDefaults.standard.set(items, forKey: "items")
////                  UserDefaults.standard.synchronize()
//
////            let dict:NSMutableDictionary = self.productlist[indexPath.row] as! NSMutableDictionary
////                                           if !GlobalVariables.globalarray.contains{ $0 == dict as! [String : Any] } {
////                                               GlobalVariables.globalarray.append(dict)
////                                           }
////                                           if !GlobalVariables.globalarray.contains(where: {$0 == dict}) {
////                                               GlobalVariables.globalarray.append(dict)
////                                           }
//        }
//
//    }

    

   // MARK: - Button Action
    
    @IBAction func back_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
//    @IBAction func cart_click(_ sender: UIButton) {
//
//        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
//        nextViewController.subcategorydictdata = subcategorydictdata
//        self.navigationController?.pushViewController(nextViewController, animated: true)
//    }
    
    // MARK: - API
       
         func LocalProductApi()
                                {
                                    if !isInternetAvailable(){
                                        noInternetConnectionAlert(uiview: self)
                                    }
                                    else
                                    {
                                       let url = ServiceList.SERVICE_URL+ServiceList.LOCAL_PRODUCT_API
                                      
//                                      let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
//                                                                 "X-SANTOSH-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
//                                                                 ] as [String : Any]
                                      
                                //        let parameters = [ :
                                //        ] as [String : Any]
                      
                                         callApi(url,
                                                       method: .get,
                                                      // param: parameters ,
                                                       //extraHeader: header,
                                                      withLoader: true)
                                               { (result) in
                                                    print("PRODUCTRESPONSE:",result)
                                                   
                                                    if result.getBool(key: "status")
                                                    {
                                                        let res = result.strippingNulls()
                                                        let data = res["data"] as? [[String : Any]]
                                                        
                                                        self.productlist = data ?? []
                                                        
//                                                        for dict in self.productlist {
//                                                        self.temparr.append("")
//                                                            self.pricetemparr.append("")
//                                                        }
                                                        
                                                       self.tblproductlist.reloadData()
                                                    }
                                                 else
                                                 {
                                                     showToast(uiview: self, msg: result.getString(key: "message"))
                                                 }
                                        }
                                    }
                                    
                                }
    
//    func OfferProductApi()
//                                   {
//                                       if !isInternetAvailable(){
//                                           noInternetConnectionAlert(uiview: self)
//                                       }
//                                       else
//                                       {
//                                          let url = ServiceList.SERVICE_URL+ServiceList.OFFER_PRODUCT_API
//
//                                         let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
//                                                                    "X-SANTOSH-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
//                                                                    ] as [String : Any]
//
//                                        let parameters = [:
//                                           ] as [String : Any]
//
//                                            callApi(url,
//                                                          method: .post,
//                                                          param: parameters ,
//                                                          extraHeader: header,
//                                                         withLoader: true)
//                                                  { (result) in
//                                                       print("PRODUCTRESPONSE:",result)
//
//                                                       if result.getBool(key: "status")
//                                                       {
//                                                        let res = result.strippingNulls()
//                                                        let data = res["data"] as? [[String : Any]]
//
//                                                          //let data = result["data"] as? [[String : Any]]
//                                                           self.productlist = data ?? []
//                                                          self.tblproductlist.reloadData()
//                                                       }
//                                                    else
//                                                    {
//                                                        showToast(uiview: self, msg: result.getString(key: "message"))
//                                                    }
//                                           }
//                                       }
//
//                                   }
    
}
