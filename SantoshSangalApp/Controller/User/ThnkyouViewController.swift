//
//  ThnkyouViewController.swift
//  1StopShopApp
//
//  Created by My Mac on 29/05/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit

class ThnkyouViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

   // MARK: - Button Action
    
    @IBAction func back_click(_ sender: UIButton) {
         let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
               self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func continue_click(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
}
