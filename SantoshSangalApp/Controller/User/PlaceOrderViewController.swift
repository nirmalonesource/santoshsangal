//
//  PlaceOrderViewController.swift
//  SantoshSangal
//
//  Created by My Mac on 25/06/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import IBAnimatable
import iOSDropDown
import M13Checkbox
import CFSDK

class PlaceOrderViewController: UIViewController {

    @IBOutlet var lblsubtotal: UILabel!
       @IBOutlet var lblDC: UILabel!
       @IBOutlet var lbltotal: UILabel!
  //  @IBOutlet var txtzipcode: AnimatableTextField!
    @IBOutlet var txtzipcode: DropDown!
    @IBOutlet var textviewaddress: AnimatableTextView!
    
    @IBOutlet var chkselfpickup: M13Checkbox!
    @IBOutlet var chkbysantosh: M13Checkbox!
    @IBOutlet var lblmsg: UILabel!
    
    var productlist = [[String:Any]]()
      var dc = Double()
     var subtotal = Double()
     var total = Double()
    
    var lblsubtotalvalue = String()
     var lbldcvalue = String()
     var lbltotalvalue = String()
     var ziparr = [String]()
    var strradio = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let chkarr = [chkselfpickup,chkbysantosh]
        
        for chk in chkarr {
            chk?.markType = .radio
            chk?.boxType = .circle
            chk?.tintColor = #colorLiteral(red: 0.3450980392, green: 0.6156862745, blue: 0.04705882353, alpha: 1)
            chk?.secondaryTintColor = UIColor.lightGray
            chk?.stateChangeAnimation = .fill
           // chk?.secondaryCheckmarkTintColor = UIColor.red
        }
        
         chkbysantosh?.checkState = .checked
        strradio = "Delivery By Santosh Sangle"
       lblmsg.text = "1) We currently offer Next day  delivery. \n\n 2) Order Place after 12 PM we will be deliver next day, but we will try to deliver on same day.\n\n 3) Free deliveries for orders worth Rs.500 & above."
        
      //  ziparr = ["400001","400002","400004","400006","400007","400008","400018","400020","400026","400034","400036"]
               
          //     txtzipcode.optionArray = ziparr
        
        ZipCodeApi()
        
        lblsubtotal.text = lblsubtotalvalue
        lblDC.text = lbldcvalue
        lbltotal.text = lbltotalvalue
        
        txtzipcode.text = UserDefaults.standard.getUserDict()["zipcode"] as? String ?? ""
        textviewaddress.text = UserDefaults.standard.getUserDict()["address"] as? String ?? ""
    }
    

    // MARK: - Button Action
    
    @IBAction func chkself_click(_ sender: M13Checkbox) {
        chkselfpickup.checkState = .checked
        chkbysantosh.checkState = .unchecked
        strradio = "Self Pickup"
         lblmsg.text = "Ground floor,\n Bldg no 34 \n Boyce Bungalow \n Opp Jakson Bank \n Next to BMC BLOG \n Seleter road Grantroad west \n MUMBAI 400007"
    }
    @IBAction func chkbysantosh_click(_ sender: M13Checkbox) {
        chkselfpickup.checkState = .unchecked
        chkbysantosh.checkState = .checked
        strradio = "Delivery By Santosh Sangle"
         lblmsg.text = "1) We currently offer Next day  delivery. \n\n 2) Order Place after 12 PM we will be deliver next day, but we will try to deliver on same day.\n\n 3) Free deliveries for orders worth Rs.500 & above."
    }
    
    @IBAction func back_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func confirm_click(_ sender: UIButton) {
        ProductOrderApi(isfrom: true)
       
    }
    @IBAction func Creditcard_click(_ sender: UIButton) {
       //  ProductOrderApi(isfrom: false)
    }
    
     // MARK: - API
    
    func ZipCodeApi()
                     {
                         if !isInternetAvailable(){
                             noInternetConnectionAlert(uiview: self)
                         }
                         else
                         {
                            
                        let parameters = [:] as [String : Any]
                          
                          let url = ServiceList.SERVICE_URL+ServiceList.ZIPCODE_API
                          
                              callApi(url,
                                      method: .get,
                                      param: parameters ,
                                      withLoader: true)
                                    { (result) in
                                        print("ZIPCODERESPONSE:",result)
                                         if result.getBool(key: "status")
                                         {
                                            let arr = result.getArrayofDictionary(key: "data")
                                            self.ziparr = arr.compactMap {"\($0["Zipcode"] as? String ?? "0")"}
                                            self.txtzipcode.optionArray = self.ziparr
                                         }
                                      else
                                         {
                                          showToast(uiview: self, msg: result.getString(key: "message"))
                                      }
                             }
                         }
                         
                     }

                 
    func ProductOrderApi(isfrom:Bool)
                                          {
                                              if !isInternetAvailable(){
                                                  noInternetConnectionAlert(uiview: self)
                                              }
                                              else
                                              {
                                                 let url = ServiceList.SERVICE_URL+ServiceList.PLACE_ORDER_API
                                                
                                                var productarr = [[String:Any]]()
                                                var json = String()
                                                for dict in productlist {
                                                    if dict["isPack"] as! String == "1" {
                                                        var singleproduct1 = [
                                                            
                                                            "CategoryID":dict["CategoryID"] ?? "",
                                                            "CategoryName":dict["CategoryName"] ?? "",
                                                            "PKTSize":dict["PKTSize"] ?? "",
                                                            "Price":dict["Price"] ?? "",
                                                            "ProductCode":dict["ProductCode"] ?? "",
                                                            "ProductID":dict["ProductID"] ?? "",
                                                            "ProductImage":dict["ProductImage"] ?? "",
                                                            "ProductName":dict["ProductName"] ?? ""
                                                            
                                                        ]
                                                        
                                                        let singleproduct2 = [
                                                            
                                                            "SubcategoryID":dict["SubcategoryID"] ?? "",
                                                            "SubcategoryName":dict["SubcategoryName"] ?? "",
                                                            "UOMDesc":dict["UOMDesc"] ?? "",
                                                            "UOMID":dict["UOMID"] ?? "",
                                                            "fromCart":false,
                                                            "isAvailable":dict["isAvailable"] ?? "",
                                                            "isPack":dict["isPack"] ?? "",
                                                            "qty":dict["qty"] ?? "",
                                                             "UOMQty": "",
                                                        ]
                                                        
                                                        singleproduct1.merge(dict: singleproduct2)
                                                        
                                                        productarr.append(singleproduct1)
                                                    }
                                                    else
                                                    {
                                                        let Details = dict["Details"] as? [[String:Any]]
                                                        var uid = String()
                                                        var price = String()
                                                        if Details?.count ?? 0 > 0 {
                                                              
                                                            for i in Details ?? []  {
                                                               let j = i["pktsize"] as! String
                                                               // let arr = j.components(separatedBy: " ")
                                                                
                                                                let k = dict["qty"] as? String ?? ""
                                                                let arr1 = k.components(separatedBy: " ")
                                                                if j == arr1[0] {
                                                                    uid = (i["uid"] as? String)!
                                                                      price = (i["price"] as? String)!
                                                                }
                                                                
                                                            }
                                                        }
                                                        
                                                        var singleproduct1 = [
                                                            "CategoryID":dict["CategoryID"] ?? "",
                                                            "CategoryName":dict["CategoryName"] ?? "",
                                                            "Price":dict["Price"] ?? "",
                                                            "ProductCode":dict["ProductCode"] ?? "",
                                                            "ProductID":dict["ProductID"] ?? "",
                                                            "ProductImage":dict["ProductImage"] ?? ""
                                                        ]
                                                        
                                                        var singleproduct2 = [
                                                           
                                                            "UOMID":dict["UOMID"] ?? "",
                                                            "fromCart":false,
                                                            "isAvailable":dict["isAvailable"] ?? "",
                                                            "isPack":dict["isPack"] ?? "",
                                                            "qty":dict["qty"] ?? "",
                                                            "UOMQty":dict["qty"] as? String ?? ""
                                                        ]
                                                        
                                                        let singleproduct3 = [
                                                            "PKTSize":dict["PKTSize"] ?? "",
                                                            "SubcategoryID":dict["SubcategoryID"] ?? "",
                                                            "SubcategoryName":dict["SubcategoryName"] ?? "",
                                                            "UOMDesc":dict["UOMDesc"] ?? "",
                                                            "ProductName":dict["ProductName"] ?? "",
                                                            "LPPrice":String(price),
                                                            "LPID":uid
                                                        ]
                                                        
                                                        
                                                        singleproduct2.merge(dict: singleproduct3)
                                                        singleproduct1.merge(dict: singleproduct2)
                                                        
                                                        productarr.append(singleproduct1)
                                                    }
                                                    
                                                }
                                                
                                                do {

                                                    //Convert to Data
                                                    let jsonData = try JSONSerialization.data(withJSONObject: productarr, options: JSONSerialization.WritingOptions.prettyPrinted)

                                                    //Convert back to string. Usually only do this for debugging
                                                    if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                                                       print(JSONString)
                                                        json = JSONString
                                                    }

                                                    //In production, you usually want to try and cast as the root data structure. Here we are casting as a dictionary. If the root object is an array cast as [Any].
    //                                                json = try (JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String: Any])!


                                                } catch {
                                                    print(error)
                                                }
                                                
                                                
                                                let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                           "X-SANTOSH-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                           ] as [String : Any]
                                                
                                                 let parameters = [ "SubTotal" : String(subtotal) ,
                                                                    "DeliveryCharge" : String(dc) ,
                                                                    "TotalPrice" : String(total),
                                                                    "zipcode" : txtzipcode.text  ?? "",
                                                                    "address" : textviewaddress.text  ?? "",
                                                                    "Products":json,
                                                                    "Pickup":strradio
                                                  ] as [String : Any]
                                
                                                   callApi(url,
                                                                 method: .post,
                                                                 param: parameters ,
                                                                 extraHeader: header,
                                                                withLoader: true)
                                                         { (result) in
                                                              print("PRODUCTRESPONSE:",result)
                                                             
                                                              if result.getBool(key: "status")
                                                              {
                                                                 let data = result["data"] as? [[String : Any]]
                                                             if isfrom
                                                             {
//                                                                UserDefaults.standard.removeObject(forKey: "items")
//                                                                self.productlist = [[String:Any]]()
//                                                                GlobalVariables.globalarray = [[String:Any]]()
//
//                                                                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ThnkyouViewController") as! ThnkyouViewController
//                                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                
//                                                                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "PaymentOptionViewController") as! PaymentOptionViewController
//                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                 self.GetTokenApi(dict: result)
                                                                }
                                                                else
                                                             {
                                                                self.GetTokenApi(dict: result)
                                                                }
                                                               
                                                              }
                                                           else
                                                           {
                                                               showToast(uiview: self, msg: result.getString(key: "message"))
                                                           }
                                                  }
                                              }
                                              
                                          }
    
   
   
    
    func GetTokenApi(dict:[String:Any])
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
        else
        {
            //let url = "https://test.cashfree.com/api/v2/cftoken/order"
            
//            let header = ["X-Client-Id":"271639142ca5f16695751fef836172",
//            "X-Client-Secret":"cc3adc1d537c83991882d9c5eb551f387177eeda" ,
//                                    ] as [String : Any]
            
            let parameters = ["orderId": dict["order_id"]!, "orderAmount": String(total),"orderCurrency":"INR"] as [String : Any]
    // let parameters = ["orderId": dict["order_id"]!, "orderAmount": String(1),"orderCurrency":"INR"] as [String : Any]

            
            
            //create the session object
             // TEST
            // https://api.cashfree.com/api/v2/cftoken/order
             // LIVE
            // https://test.cashfree.com/api/v2/cftoken/order
            
                       let session = URLSession.shared
                let url = URL(string: "https://test.cashfree.com/api/v2/cftoken/order")! //change the url
                       //now create the URLRequest object using the url object
                       var request = URLRequest(url: url)
                       request.httpMethod = "POST" //set http method as POST

                       do {
                           request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
                       } catch let error {
                           print(error.localizedDescription)
                       }

                       request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                       request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            // TEST
                   request.addValue("271639142ca5f16695751fef836172", forHTTPHeaderField: "X-Client-Id")
                   request.addValue("cc3adc1d537c83991882d9c5eb551f387177eeda", forHTTPHeaderField: "X-Client-Secret")
            // LIVE
//            request.addValue("71491b1fd62868e182e2cb9ad19417", forHTTPHeaderField: "X-Client-Id")
//            request.addValue("acaa9ea5a311b8e7f3f14e0d01728bc50620dfbd", forHTTPHeaderField: "X-Client-Secret")

                       //create dataTask using the session object to send data to the server
                       let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

                           guard error == nil else {
                               return
                           }

                           guard let data = data else {
                               return
                           }

                           do {
                               //create json object from data
                               if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                                   print(json)
                                   
                              let paymentparam =     [
                                "orderId": String(dict["order_id"] as! Int),
                                "tokenData" : json["cftoken"],
                                "orderAmount":  String(self.total),
                                "customerName": "Customer Name",
                                "orderNote": "Order Note",
                                "orderCurrency": "INR",
                                "customerPhone": "9012341234",
                                "customerEmail": "sample@gmail.com"
                    
                              //  "notifyUrl": "https://test.gocashfree.com/notify"
                            ]  as! Dictionary<String, String>
                            // handle json...
                            DispatchQueue.main.async {
                                
                                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "PaymentOptionViewController") as! PaymentOptionViewController
                                nextViewController.paymentparam = paymentparam
                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                
//                                let cfViewController = CFViewController (
//                                    params: paymentparam ,
//                                    appId: self.appId,
//                                    env: self.environment,
//                                    callBack: self)
//                                self.navigationController?.pushViewController (cfViewController, animated: true);
                                }
                               }
                           } catch let error {
                               print(error.localizedDescription)
                           }
                       })
                       task.resume()
            
        }
        
    }
             
    
}


