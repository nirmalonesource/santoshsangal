//
//  PaymentOptionViewController.swift
//  SantoshSangal
//
//  Created by My Mac on 23/11/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import CFSDK
class PaymentOptionViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    var menuarr = [String]()
    var menuimgarr = [String]()
    var paymentparam: [String : String] = [:]
    
      // appId from your merchant dashboard.
      
      // TEST
       let environment = "TEST"
    let appId = "271639142ca5f16695751fef836172"
      
      // LIVE
//       let environment = "PROD"
//       let appId = "71491b1fd62868e182e2cb9ad19417"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        menuarr = ["Credit / Debit Card","Net Banking","Google Pay","PhonePe","Cash On Delivery"]
        menuimgarr = ["creditcard","netbanking","googlepe","phonepe","cod"]
        
       
    }
    
    //MARK: - Tableview Data Source
       
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return menuarr.count
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: "sidemenutablecell", for: indexPath) as! sidemenutablecell
          tableView.tableFooterView = UIView()
           
            cell.lbltitle.text = menuarr[indexPath.row]
           let imageName = menuimgarr[indexPath.row]
           cell.imgicon.image = UIImage(named: imageName)
           
           //cell.imgicon.changeImageViewImageColor(color: #colorLiteral(red: 0.3450980392, green: 0.6156862745, blue: 0.04705882353, alpha: 1))
           //cell.imgicon.image = UIImage(named: menuimgarr[indexPath.row])
           cell.selectionStyle = .none
           return cell
       }
       
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           let indexPath = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example

           let currentCell = tableView.cellForRow(at: indexPath!) as! sidemenutablecell
         
                   if currentCell.lbltitle.text == "Credit / Debit Card"
                   {
                                             let cfViewController = CFViewController (
                                                 params: paymentparam ,
                                                 appId: self.appId,
                                                 env: self.environment,
                                                 callBack: self)
                                             self.navigationController?.pushViewController (cfViewController, animated: true);
                   }
                       else  if currentCell.lbltitle.text == "Net Banking"
                       {
                          let cfViewController = CFViewController (
                                params: paymentparam ,
                                appId: self.appId,
                                env: self.environment,
                                callBack: self)
                            self.navigationController?.pushViewController (cfViewController, animated: true);
                       }
                       
                       else  if currentCell.lbltitle.text == "Google Pay"
                       {
                       
                        let new = ["appName":"CFUPIApp.GPAY"]
//                        paymentparam.merge(dict: new)
//                        print(paymentparam)
                           let cfViewController = CFViewController (
                                params: paymentparam ,
                                appId: self.appId,
                                env: self.environment,
                                callBack: self)
                            self.navigationController?.pushViewController (cfViewController, animated: true);
                       }
                       
                       else  if currentCell.lbltitle.text == "PhonePe"
                       {
                        let cfViewController = CFViewController (
                            params: paymentparam ,
                            appId: self.appId,
                            env: self.environment,
                            callBack: self)
                        self.navigationController?.pushViewController (cfViewController, animated: true);
                          
                       }
                        else  if currentCell.lbltitle.text == "Cash On Delivery"
                        {
//                            map.put("orderId",orderId );
//                            map.put("paymentMode",paymentMode);
                            
                            UserDefaults.standard.removeObject(forKey: "items")
                                                //  self.productlist = [[String:Any]]()
                                                  GlobalVariables.globalarray = [[String:Any]]()
                                              //  self.tblproductlist.reloadData()
                                              //  self.calcamt()
                                              
                                              let parameters = [
                                                "UserId" : UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
                                                "orderId" : paymentparam["orderId"]! ,
                                                "orderAmount" : paymentparam["orderAmount"]!,
                                                "paymentMode" : "COD",
                                                "referenceId" : "",
                                                "txStatus" : "",
                                                "txMsg" : "",
                                                "txTime" : "",
                                                "type" : "CashFreeResponse",
                                                "signature" : ""
                                              ] as [String : Any]
                                              
                                              self.PaymentDetailApi(parameters: parameters)
                        }
    }
    

  // MARK: - Button Action
    @IBAction func back_click(_ sender: UIButton) {
           self.navigationController?.popViewController(animated: true)
       }
    
    func PaymentDetailApi(parameters:[String:Any])
                             {
                                 if !isInternetAvailable(){
                                     noInternetConnectionAlert(uiview: self)
                                 }
                                 else
                                 {
                                    let url = ServiceList.SERVICE_URL+ServiceList.PAYMENT_DETAIL_API
                                   
                                   let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                              "X-SANTOSH-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                              ] as [String : Any]
                                   
                   
                                      callApi(url,
                                                    method: .post,
                                                    param: parameters ,
                                                    extraHeader: header,
                                                   withLoader: true)
                                            { (result) in
                                                 print("PRODUCTRESPONSE:",result)
                                                
                                                 if result.getBool(key: "status")
                                                 {
                                                    // let res = result.strippingNulls()
                                                   //  let data = res["data"] as? [[String : Any]]
                                                  let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ThnkyouViewController") as! ThnkyouViewController
                                                  self.navigationController?.pushViewController(nextViewController, animated: true)
                                                 }
                                              else
                                              {
                                                  showToast(uiview: self, msg: result.getString(key: "message"))
                                              }
                                     }
                                 }
                                 
                             }
    
}


extension PaymentOptionViewController : ResultDelegate {
    

    // This is Struct for the result (See viewDidAppear)
    struct Result : Codable {
        let orderId: String
        let referenceId: String
        let orderAmount: String
        let txStatus: String
        let txMsg: String
        let txTime: String
        let paymentMode: String
        let signature: String
        
        enum CodingKeys : String, CodingKey {
            case orderId
            case referenceId
            case orderAmount
            case txStatus
            case txMsg
            case txTime
            case paymentMode
            case signature
        }
    }
    // End of Struct for the result
    
    func onPaymentCompletion(msg: String) {
        print("JSON value : \(msg)")
        let inputJSON = "\(msg)"
        let inputData = inputJSON.data(using: .utf8)!
        let decoder = JSONDecoder()
        if inputJSON != "" {
            do {
                let result2 = try decoder.decode(Result.self, from: inputData)
                print(result2.orderId)
                print(result2)
                
                if result2.txStatus == "SUCCESS" {
                    
                    UserDefaults.standard.removeObject(forKey: "items")
                      //  self.productlist = [[String:Any]]()
                        GlobalVariables.globalarray = [[String:Any]]()
                    //  self.tblproductlist.reloadData()
                    //  self.calcamt()
                    
                    let parameters = ["UserId" : UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
                                      "orderId" : result2.orderId ,
                                   "orderAmount" : result2.orderAmount,
                                   "paymentMode" : result2.paymentMode,
                                   "referenceId" : result2.referenceId,
                                   "txStatus" : result2.txStatus,
                                   "txMsg" : result2.txMsg,
                                   "txTime" : result2.txTime,
                                   "type" : "CashFreeResponse",
                                   "signature" : result2.signature
                                           
                    ] as [String : Any]
                    
                    self.PaymentDetailApi(parameters: parameters)
                }
                else if result2.txStatus == "CANCELLED" {
                    showToast(uiview: self, msg: "TRANSACTION CANCELLED")
                }
                else
                {
                    showToast(uiview: self, msg: result2.txStatus)
                }
                
                
            } catch {
                // handle exception
                print("BDEBUG: Error Occured while retrieving transaction response")
            }
        } else {
            print("BDEBUG: transactionResult is empty")
        }
    }
    

}
