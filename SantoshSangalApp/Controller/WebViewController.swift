//
//  WebViewController.swift
//  1StopShopApp
//
//  Created by My Mac on 09/06/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var webvw: WKWebView!
    @IBOutlet var textvw: UITextView!
    var str = String()
    var isfrom = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if isfrom == "terms" {
            str = "1) We currently offer Next day  delivery. \n\n 2) Order Place after 12 PM we will be deliver next day, but we will try to deliver on same day.\n\n 3) Free deliveries for orders worth Rs.500 & above"
            lbltitle.text = "Terms & Conditions"
        }
        else
        {
            str = "Contact Us  At \n\n\n Whats App :9821781104 \n\n Mobile: 8108671885 \n\n\n Santosh Sangle/Chandrakant Sangle \n\n\n Address \n\n Ground floor,\n Bldg no 34 \n Boyce Bungalow \n Opp Jakson Bank \n Next to BMC BLOG \n Seleter road Grantroad west \n MUMBAI 400007"
            
            lbltitle.text = "Support"
        }
        

       // webvw.loadHTMLString(str.replacingOccurrences(of: "\n", with: "<br/>"), baseURL: nil)
        
     //   webvw.loadHTMLString(str, baseURL: nil)
        
//        let url = URL(string: "https://www.facebook.com/")
//        webvw.load(URLRequest(url: url!))
        
        textvw.text = str
    }
    

    // MARK: - Button Action
    
    @IBAction func back_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}
