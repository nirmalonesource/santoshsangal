//
//  DeliveryPersonViewController.swift
//  SantoshSangal
//
//  Created by My Mac on 01/07/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import IBAnimatable

class deliverypersoncell: UITableViewCell {
    @IBOutlet var lblname: UILabel!
    @IBOutlet var lblmono: UILabel!
    @IBOutlet var activeswtch: UISwitch!
    
}

class DeliveryPersonViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate {

    @IBOutlet var tblpersonlist: UITableView!
    
    var PersonList = [[String:Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
        tblpersonlist.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
         PersonsApi()
    }
    
    //MARK: - Tableview Data Source
                    
                    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                     
                       return PersonList.count
                       
                    }
                    
                    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                     
                        let cell = tableView.dequeueReusableCell(withIdentifier: "deliverypersoncell", for: indexPath) as! deliverypersoncell
                       let dict:[String:Any] = PersonList[indexPath.row]
                       cell.lblname.text = "\(dict["first_name"] as? String ?? "") \(dict["last_name"] as? String ?? "")"
                        
                        cell.lblmono.attributedText = (dict["mobile_no"] as? String ?? "").getUnderLineAttributedText()
                        
                        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(labelAction(gr:)))
                        cell.lblmono.addGestureRecognizer(tap)
                        tap.delegate = self //
                        
                        if dict["activated"] as? String ?? "" == "1"{
                                    cell.activeswtch.isOn = true
                                       }
                                       else
                                        {
                                           cell.activeswtch.isOn = false
                                       }
                                       cell.activeswtch.tag = indexPath.row
                                    //   cell.activeswtch.addTarget(self, action: Selector(("switchTriggered:")), for: .valueChanged );
                                       cell.activeswtch.addTarget(self, action:#selector(switchTriggered(sender:)), for: .valueChanged)
                       
                        return cell
                    }
               
               
               
               func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                   
//                   let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
//                   nextViewController.OrderDict = OrderList[indexPath.row]
//                   self.navigationController?.pushViewController(nextViewController, animated: true)
               }
    
    @objc func labelAction(gr:UITapGestureRecognizer)
    {
        let searchlbl:UILabel = (gr.view as! UILabel) // Type cast it with the class for which you have added gesture
        print(searchlbl.text ?? "")
       if let url = URL(string: "tel://\(searchlbl.text ?? "")"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @objc func switchTriggered(sender: UISwitch) {

        let swtch = sender
        
        if swtch.isOn {
            ActivePersonApi(persontid: PersonList[sender.tag]["id"] as? String ?? "", active: "1")
        }
        else
        {
            ActivePersonApi(persontid: PersonList[sender.tag]["id"] as? String ?? "", active: "0")
        }
        
    }
       
       
       // MARK: - API
    
    func ActivePersonApi(persontid:String,active:String)
                    {
                        if !isInternetAvailable(){
                            noInternetConnectionAlert(uiview: self)
                        }
                        else
                        {
                           let url = ServiceList.SERVICE_URL+ServiceList.SALES_PERSONS_ACTIVE_INACTIVE_API
                          
                          let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                     "X-SANTOSH-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                     ] as [String : Any]
                          
                           let parameters = [// "category_id" : subcategorydictdata["CategoryID"]! ,
                                           //   "sub_category_id" : subcategorydictdata["SubcategoryID"]! ,
                                              "id" : persontid,
                                              "active":active
                            ] as [String : Any]
          
                             callApi(url,
                                           method: .post,
                                           param: parameters ,
                                           extraHeader: header,
                                          withLoader: true)
                                   { (result) in
                                        print("PRODUCTRESPONSE:",result)
                                       
                                        if result.getBool(key: "status")
                                        {
      //                                      let res = result.strippingNulls()
      //                                      let data = res["data"] as? [[String : Any]]
      //                                      self.productlist = data ?? []
      //                                     self.tblproductlist.reloadData()
                                          self.PersonsApi()
                                        }
                                     else
                                     {
                                         showToast(uiview: self, msg: result.getString(key: "message"))
                                     }
                            }
                        }
                        
                    }
             
               func PersonsApi()
                                      {
                                          if !isInternetAvailable(){
                                              noInternetConnectionAlert(uiview: self)
                                          }
                                          else
                                          {
                                             let url = ServiceList.SERVICE_URL+ServiceList.SALES_PERSON_API
                                            
                                            let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                       "X-SANTOSH-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                       ] as [String : Any]
                                            
                                           let parameters = [ :
                                              ] as [String : Any]
                            
                                               callApi(url,
                                                             method: .get,
                                                             param: parameters ,
                                                             extraHeader: header,
                                                            withLoader: true)
                                                     { (result) in
                                                          print("PERSONRESPONSE:",result)
                                                         
                                                          if result.getBool(key: "status")
                                                          {
                                                             let data = result["data"] as? [[String : Any]]
                                                              self.PersonList = data ?? []
                                                             self.tblpersonlist.reloadData()
                                                          }
                                                       else
                                                       {
                                                           showToast(uiview: self, msg: result.getString(key: "message"))
                                                       }
                                              }
                                          }
                                          
                                      }
    

   // MARK: - Button Action
   
   @IBAction func back_click(_ sender: UIButton) {
       self.navigationController?.popViewController(animated: true)
   }
    @IBAction func add_click(_ sender: AnimatableButton) {
           
           let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddPersonViewController") as! AddPersonViewController
                //  nextViewController.subcategorydictdata = subcategorydictdata
           self.navigationController?.pushViewController(nextViewController, animated: true)
       }

}
