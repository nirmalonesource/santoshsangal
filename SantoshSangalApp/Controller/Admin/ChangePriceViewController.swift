//
//  ChangePriceViewController.swift
//  SantoshSangal
//
//  Created by My Mac on 27/06/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import iOSDropDown
import IBAnimatable

class changepricecell: UITableViewCell {
    @IBOutlet var imgphoto: UIImageView!
    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var lblmainprice: UILabel!
    @IBOutlet var lbldiscprice: UILabel!
    
    @IBOutlet var txtmainprice: UITextField!
    @IBOutlet var txtdiscprice: UITextField!
    
    
}


class ChangePriceViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    
    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var btncart: SSBadgeButton!
    @IBOutlet var tblproductlist: UITableView!
    
    var temparr = [String]()
    var tempdiscarr = [String]()
    var editedproductlist = [[String:Any]]()
     var productlist = [[String:Any]]()
    var  subcategorydictdata = [String:Any]()
    var quntyarr = (1...50).map{"\($0)"}
    var isfromposter = false
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      //  lbltitle.text = subcategorydictdata["SubcategoryName"] as? String
        
        tblproductlist.tableFooterView = UIView()
       // btncart.badge = String(GlobalVariables.globalarray.count)
      //  btncart.badge = String(99)
      //  btncart.addBadge(number: 99, addedView: btncart)
        if isfromposter {
            OfferProductApi()
        }
        else
        {
            ProductApi()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
         btncart.badge = String(GlobalVariables.globalarray.count)
    }
    
   
    
        //MARK: - Tableview Data Source
             
             func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
              
                return productlist.count
                
             }
             
             func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
              
                 let cell = tableView.dequeueReusableCell(withIdentifier: "changepricecell", for: indexPath) as! changepricecell
                
                cell.lbltitle.text = productlist[indexPath.row]["ProductName"] as? String
                
               
//                cell.lblprice.attributedText = nil
//                 cell.lblprice.text = nil
                
                if productlist[indexPath.row]["isDealOfDay"] as? String ?? "" == "1"{
                    
//                    cell.txtdiscprice.text = productlist[indexPath.row]["Dis_price"] as? String ?? "" //
//                    cell.txtmainprice.text = productlist[indexPath.row]["Price"] as? String ?? ""
                    
//                    cell.txtdiscprice.isHidden = false
//                    cell.lbldiscprice.isHidden = false
                }
                else
                {
                   // cell.lblofferprice.text = ""
                  //  cell.txtmainprice.text = productlist[indexPath.row]["Price"] as? String ?? "" //"RS \(productlist[indexPath.row]["Price"] as? String ?? "")"
//                    cell.txtdiscprice.isHidden = true
//                    cell.lbldiscprice.isHidden = true
                }
                
                
                let imgURL = productlist[indexPath.row]["ProductImage"] as? String ?? ""
                                     cell.imgphoto.sd_setImage(with: URL(string:imgURL), placeholderImage: #imageLiteral(resourceName: "Logo"), options:.progressiveLoad, completed: nil)
                
               // cell.btnaddcart.tag = indexPath.row
               
                cell.txtmainprice.tag = indexPath.row
                 cell.txtmainprice.delegate = self
                
                cell.txtdiscprice.tag = indexPath.row
                cell.txtdiscprice.delegate = self
                
                cell.txtmainprice.addTarget(self, action: #selector(typingmainprice), for: .editingChanged)
                cell.txtdiscprice.addTarget(self, action: #selector(typingdiscprice), for: .editingChanged)
                
                 cell.txtmainprice.text = temparr[indexPath.row]
                cell.txtdiscprice.text = tempdiscarr[indexPath.row]
                
                cell.lblmainprice.text = "Price (\(productlist[indexPath.row]["UOMDesc"] as? String ?? "" ))"
                
//                let isIndexValid = temparr.indices.contains(indexPath.row)
//                if isIndexValid {
//                    cell.txtmainprice.text = temparr[indexPath.row]
//                }
//                else
//                {
//                     cell.txtmainprice.text = productlist[indexPath.row]["Price"] as? String ?? ""
//                }
//
//                let isIndexValidDisc = tempdiscarr.indices.contains(indexPath.row)
//                if isIndexValidDisc {
//                    cell.txtdiscprice.text = tempdiscarr[indexPath.row]
//                }
//                else
//                {
//                     cell.txtdiscprice.text = productlist[indexPath.row]["Dis_price"] as? String ?? ""
//                }
                
//                cell.btnaddcart.addTarget(self, action:#selector(addcartclick(_:event:)), for: .touchUpInside)
                
//                 cell.btnCartAction = { () in
//
//                   // print("QNTY:",cell.txtqty.text ?? "")
//                     print("Deletecell:",indexPath.row)
//
//                    self.popupAlert(title: "", message: "Are you sure want to Delete?", actionTitles: ["NO","YES"], actions:[{action1 in
//
//                    },{action2 in
//                          self.DeleteProductApi(productid: self.productlist[indexPath.row]["ProductID"] as? String ?? "")
//                    }, nil])
//
//
//
//                }
                
//                cell.btnEditAction = { () in
//
//                               // print("QNTY:",cell.txtqty.text ?? "")
//                                 print("Editcell:",indexPath.row)
//
//
//                            }
                
                let Details = productlist[indexPath.row]["Details"] as? [[String:Any]]
                
//                if Details?.count ?? 0 > 0 {
//                    cell.lbltotalprice.isHidden = false
////                    cell.txtqty.optionArray =  Details!.compactMap {"\( $0["pktsize"] as? String ?? "0") \( $0["uomdesc"] as? String ?? "0")"}
////                     cell.txtqty.isSearchEnable = false
//
//                }
//                else
//                {
////                    cell.txtqty.optionArray =  quntyarr
////                    cell.txtqty.isSearchEnable = true
//                      cell.lbltotalprice.isHidden = true
//                }
                
                
              //  cell.txtqty.optionIds =  quntyarr.compactMap {Int( $0["CategoryID"] as? String ?? "0")}
//   cell.txtqty.didSelect(completion: { (selected, index, id)  in
//          print(index,id)
//    print("cell:",indexPath.row)
//    if Details?.count ?? 0 > 0
//    {
//        cell.lbltotalprice.text = "RS \(Details?[index]["price"] as? String ?? "0")"
//    }
//    else
//    {
//    }
//      })
                 return cell
    }
        
//        func insertElementAtIndex(element: String?, indexx: Int) {
//
////            while temparr.count <= indexx {
////                temparr.append("")
////            }
//
//            temparr.insert(element!, at: indexx)
//        }
//
//    func DiscinsertElementAtIndex(element: String?, indexx: Int) {
//
////               while tempdiscarr.count <= indexx {
////                   tempdiscarr.append("")
////               }
//
//               tempdiscarr.insert(element!, at: indexx)
//           }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
//                     let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SubCategoryViewController") as! SubCategoryViewController
//                              nextViewController.dictdata = productlist[indexPath.row]
//                    self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    
    @objc func typingmainprice(textField:UITextField){
        let dict = productlist[textField.tag] as [String:Any]
        
        
       
        if let typedText = textField.text {
         //  self.insertElementAtIndex(element:typedText, indexx: textField.tag)
            temparr.insert(typedText, at: textField.tag)
            
            let singleproduct1 = [
                                          "ProductID":dict["ProductID"] ?? "",
                                          "Price":temparr[textField.tag] ,
                                          "Dis_price":tempdiscarr[textField.tag],
                                ]
            
            let index = editedproductlist.firstIndex(where: { dictionary in
                guard let value = dictionary["ProductID"] as? String
                else { return false }
                return value == dict["ProductID"] as? String
            })
            if let index = index {
                editedproductlist.remove(at: index)
            editedproductlist.append(singleproduct1)
            }
             else
            {
            editedproductlist.append(singleproduct1)
             }
        }
    }
    
    @objc func typingdiscprice(textField:UITextField){

         let dict = productlist[textField.tag] as [String:Any]
        if let typedText = textField.text {
         //  self.DiscinsertElementAtIndex(element: typedText, indexx: textField.tag)
            tempdiscarr.insert(typedText, at: textField.tag)
            
            let singleproduct1 = [
                                          "ProductID":dict["ProductID"] ?? "",
                                          "Price":temparr[textField.tag] ,
                                          "Dis_price":tempdiscarr[textField.tag],
                                ]
            
            let index = editedproductlist.firstIndex(where: { dictionary in
                guard let value = dictionary["ProductID"] as? String
                else { return false }
                return value == dict["ProductID"] as? String
            })
            if let index = index {
                editedproductlist.remove(at: index)
            editedproductlist.append(singleproduct1)
            }
             else
            {
            editedproductlist.append(singleproduct1)
             }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       
    }
    
   
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
//        let indexPath = IndexPath(row: textField.tag, section: 0)
//               let cell = tblproductlist.cellForRow(at: indexPath) as! changepricecell
//
//               if textField == cell.txtmainprice
//                      {
//                          self.insertElementAtIndex(element: textField.text, indexx: textField.tag)
//                      }
//                      else
//                      {
//                          self.DiscinsertElementAtIndex(element: textField.text, indexx: textField.tag)
//                      }
       
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
           let compSepByCharInSet = string.components(separatedBy: aSet)
           let numberFiltered = compSepByCharInSet.joined(separator: "")
        
       
        
           return string == numberFiltered
    }
    
    
      @objc func addcartclick(_ sender: UIButton, event: Any){
        
    }

    

   // MARK: - Button Action
    
    @IBAction func back_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func cart_click(_ sender: UIButton) {
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
        nextViewController.subcategorydictdata = subcategorydictdata
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func add_click(_ sender: AnimatableButton) {
        
        ChangeProductPriceApi()
    }
    
    // MARK: - API
    
    func ChangeProductPriceApi()
                                  {
                                      if !isInternetAvailable(){
                                          noInternetConnectionAlert(uiview: self)
                                      }
                                      else
                                      {
                                         let url = ServiceList.SERVICE_URL+ServiceList.CHANGE_PRODUCT_PRICE_API
                                        
                                        let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                   "X-SANTOSH-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                   ] as [String : Any]
                                        
                                      //  let productarr = [[String:Any]]()
                                         var json = String()
                                        
                                        
//                                        for i in 0..<temparr.count {
//                                            //YOUR LOGIC....
//
//                                            let singleproduct1 = [
//                                                "ProductID":productlist[i]["ProductID"] ?? "",
//                                                "Price":temparr[i] ,
//                                                "Dis_price":tempdiscarr[i],
//                                            ]
//                                             productarr.append(singleproduct1)
//                                        }

                                        
//                                        for dict in productlist {
//                                            if !temparr.contains(dict["Price"] as! String) || !tempdiscarr.contains(dict["Dis_price"] as! String) {
//                                                let index = temparr.firstIndex(of: "")
//                                                let singleproduct1 = [
//                                                    "ProductID":dict["ProductID"] ?? "",
//                                                    "Price":dict["Price"] ?? "",
//                                                    "Dis_price":dict["Dis_price"] ?? "",
//                                                ]
//                                                 productarr.append(singleproduct1)
//                                                }
//
//                                            }
                                        
                do {

                    //Convert to Data
                    let jsonData = try JSONSerialization.data(withJSONObject: editedproductlist, options: JSONSerialization.WritingOptions.prettyPrinted)

                    //Convert back to string. Usually only do this for debugging
                    if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                    print(JSONString)
                        json = JSONString
                    }

                    //In production, you usually want to try and cast as the root data structure. Here we are casting as a dictionary. If the root object is an array cast as [Any].
                                        //                                                json = try (JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String: Any])!


                    } catch {
                        print(error)
                    }

                                            
                                        
                                         let parameters = [// "category_id" : subcategorydictdata["CategoryID"]! ,
                                                         //   "sub_category_id" : subcategorydictdata["SubcategoryID"]! ,
                                                            "Products" : json
                                          ] as [String : Any]
                        
                                           callApi(url,
                                                         method: .post,
                                                         param: parameters ,
                                                         extraHeader: header,
                                                        withLoader: true)
                                                 { (result) in
                                                      print("PRODUCTRESPONSE:",result)
                                                     
                                                      if result.getBool(key: "status")
                                                      {
                                                        showToast(uiview: self, msg: result.getString(key: "message"))
                                                        self.ProductApi()
                                                      }
                                                   else
                                                   {
                                                       showToast(uiview: self, msg: result.getString(key: "message"))
                                                   }
                                          }
                                      }
                                      
                                  }
    
         func ProductApi()
                                {
                                    if !isInternetAvailable(){
                                        noInternetConnectionAlert(uiview: self)
                                    }
                                    else
                                    {
                                       let url = ServiceList.SERVICE_URL+ServiceList.PRODUCT_API
                                      
                                      let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                 "X-SANTOSH-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                 ] as [String : Any]
                                      
                                       let parameters = [// "category_id" : subcategorydictdata["CategoryID"]! ,
                                                       //   "sub_category_id" : subcategorydictdata["SubcategoryID"]! ,
                                                          "search" : ""
                                        ] as [String : Any]
                      
                                         callApi(url,
                                                       method: .post,
                                                       param: parameters ,
                                                       extraHeader: header,
                                                      withLoader: true)
                                               { (result) in
                                                    print("PRODUCTRESPONSE:",result)
                                                   
                                                    if result.getBool(key: "status")
                                                    {
                                                        let res = result.strippingNulls()
                                                        let data = res["data"] as? [[String : Any]]
                                                        self.productlist = data ?? []
                                                        self.temparr = [String]()
                                                        self.tempdiscarr = [String]()
                                                        for dict in self.productlist {
                                                            self.temparr.append(dict["Price"] as! String)
                                                            self.tempdiscarr.append(dict["Price"] as! String)
                                                        
                                                        }
                                                       self.tblproductlist.reloadData()
                                                    }
                                                 else
                                                 {
                                                     showToast(uiview: self, msg: result.getString(key: "message"))
                                                 }
                                        }
                                    }
                                    
                                }
    
    func OfferProductApi()
                                   {
                                       if !isInternetAvailable(){
                                           noInternetConnectionAlert(uiview: self)
                                       }
                                       else
                                       {
                                          let url = ServiceList.SERVICE_URL+ServiceList.OFFER_PRODUCT_API
                                         
                                         let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                    "X-SANTOSH-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                    ] as [String : Any]
                                         
                                        let parameters = [:
                                           ] as [String : Any]
                         
                                            callApi(url,
                                                          method: .post,
                                                          param: parameters ,
                                                          extraHeader: header,
                                                         withLoader: true)
                                                  { (result) in
                                                       print("PRODUCTRESPONSE:",result)
                                                      
                                                       if result.getBool(key: "status")
                                                       {
                                                        let res = result.strippingNulls()
                                                        let data = res["data"] as? [[String : Any]]

                                                          //let data = result["data"] as? [[String : Any]]
                                                           self.productlist = data ?? []
                                                          self.tblproductlist.reloadData()
                                                       }
                                                    else
                                                    {
                                                        showToast(uiview: self, msg: result.getString(key: "message"))
                                                    }
                                           }
                                       }
                                       
                                   }
    
}
